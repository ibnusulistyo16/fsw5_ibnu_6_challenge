'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users',[
      {
        "username":"ibnu",
        "email":"ibnu@gmail.com",
        "password":"123abc",
        "level":"admin",
        "createdAt": new Date(),
        "updatedAt":new Date()
      },
      {
        "username":"joko",
        "email":"joko@gmail.com",
        "password":"123456",
        "level":"admin",
        "createdAt": new Date(),
        "updatedAt":new Date()
      },
      {
        "username":"abcd",
        "email":"abcd@gmail.com",
        "password":"abcdefg",
        "level":"member",
        "createdAt": new Date(),
        "updatedAt":new Date()
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {})
  }
};
