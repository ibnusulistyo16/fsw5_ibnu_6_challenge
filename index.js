if(process.env.NODE_ENV !=='production'){
    require('dotenv').config()
}

const express = require('express');
const morgan = require('morgan');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const app = express();
const db = require('./models/index')

const session    = require('express-session')

const TWO_HOURS = 1000*60*60*2;
const {
    PORT = 4000,
    NODE_ENV = 'development',

    SESS_NAME ='sid',
    SESS_SECRET = 'ssh!quiet,it\'asecret',
    SESS_LIFETIME = TWO_HOURS
} = process.env
const IN_PROD = NODE_ENV ==='production'


//middleware
app.use(morgan('dev'))
app.use(express.json())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(methodOverride('_method'))
app.use(express.static(__dirname + '/'));

//middleware session start
app.use(session({
    name:SESS_NAME,
    resave:false,
    saveUninitialized:false,
    secret:SESS_SECRET,
    cookie:{
        maxAge: SESS_LIFETIME,
        sameSite: true,
        secure: IN_PROD
    }
}))
//session end

app.set('view engine','ejs');


//router
const routers= require("./routes/router") //router utama menampilkan halaman
const usersRoutes = require("./routes/users.route") //api login, crud admin 
const dashboardRoutes =  require("./routes/dashboard.route")// router menampilkan dashboard
const gameRoutes = require("./routes/game.route")
// const adminRoutes = require("./routes/user_bio.route") // api crud (user levl, biodata), read delete (history)
// const memberRoutes =require("./routes/user_history.route") // api game create read (history)
//function

app.use("/",routers)  //router index, login, signup
app.use("/users", usersRoutes) // api login , mengantar ke dashboard(admin), game(member)
app.use("/dashboard", dashboardRoutes) //router dashboard
app.use("/game",gameRoutes)  // router game

//error handling
app.use((req,res, next)=>{
    const error = new ("Not Found!");
    error.status=404;
    next(error);
})

app.use((error, req, res, next)=>{
    res.status(error.status|| 500);
    res.json({
        error:{
            message:error.message,
        }
    })
})

app.listen(PORT, ()=>{
    console.log(`Server Listening in port ${PORT}`)
})
