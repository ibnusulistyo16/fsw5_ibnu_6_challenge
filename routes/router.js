const express = require('express');
const router = express.Router();
const db = require('../models/index');

//TEMPAT ROUTER JIKA API POS ADA DI=> USERS.ROUTE.JS
const redirectLogin = (req,res,next)=>{
    if(!req.session.userId){
        res.redirect('/login')
    }else{
        next();
    }
}

const redirectDashboard = (req,res,next)=>{
    if(req.session.userId){
        res.redirect('/dashboard')
    }else{
        next();
    }
}

router.get('/', (req, res) => {
    const {userId} = req.session
    res.render('index',{userId});
})

router.get('/login',redirectDashboard, (req, res) => { // api masuk ke /user/login
    res.render('login');
})

router.get('/signup',redirectDashboard, (req, res) => { //api masuk ke /user/register
    res.render('signup1');
})

router.get('/dashboard',redirectLogin, (req, res) => {
    const {userId} = req.session
    db.Users.findAndCountAll({
        include:
        [{
            model: db.User_bio
        }]
    }).then(user=>{
        res.render('dash', { user, userId });
    }).catch((err) => {
    res.status(400).json({
        message: err.message
    });
})
})

router.get('/game',redirectLogin, (req, res) => {
    const userId = req.session.userId;
    db.Users.findOne({
        where:{
            id:userId
        }
    }).then(user=>{
        console.log(`user id /game nya : ${userId}`) 
        console.log(`username dari id : ${user.username}`)
        res.render('rps_game',{user});
    })
})

router.get('/logout', redirectLogin,(req,res)=>{
    req.session.destroy(err=>{
        if(err){
            res.redirect('/')
        }
        res.clearCookie('sid')
        res.redirect('/')
    })
})

module.exports = router;