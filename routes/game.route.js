const express = require('express');
const router = express.Router()
const db = require("../models/index")

router.get('/', (req, res) => {
    res.render('rps_game');
})

router.post('/',(req,res)=>{
    const {user_id, score_user, score_comp, status} = req.body
    console.log(req.body)
    db.User_history.create({
        user_id,
        score_user, 
        score_comp, 
        status
    }
    ).then( (user) => {
            return res.redirect('/game');
        });
})

// game/history:id
router.get('/history',(req,res)=>{
    res.redirect('/history/:id')//  
})
module.exports = router