const express = require('express');
const router = express.Router()
const db = require("../models/index")

router.get('/users',(req,res)=>{
    db.Users.findAll({
        include: 
        [{  
            model: db.User_bio
        }]
    })
    .then((user) => {
        res.render('user', { user });
    })
    .catch((err) => {
        res.status(400).json({
            message: err.message
        });
    })
})

router.get('/bio',(req,res)=>{
    db.User_bio.findAll({
        include: 
        [{  
            model: db.Users
        }]
    })
    .then((user) => {
        // res.json({user})
        res.render('bio', { user });
    })
    .catch((err) => {
        res.status(400).json({
            message: err.message
        });
    })
})

router.get('/history',(req,res)=>{
    db.User_history.findAll({
        include:
        [{
            model: db.Users
        }]
    })
    .then((user) => {
        // res.json({user})
        res.render('history', { user });
    })
    .catch((err) => {
        res.status(400).json({
            message: err.message
        });
    })
})

router.post('/users',(req,res)=>{
    const {first_name, last_name, kota, email, username, password, level} = req.body
    db.Users.create({
        email,
        username,
        password,
        level,
        User_bio:{
                first_name,
                last_name,
                kota
            }
        },
        {
            include:[{
                model:db.User_bio
            }]
        }
    ).then( (user) => {
            return res.redirect('/dashboard/users');
        }).catch(error => {
            console.log(error);
            res.redirect('/signup');
        });
})

router.put('/users/:id',(req,res)=>{
    const {email, username, password, level} = req.body
    db.Users.update({
        email,
        username,
        password,
        level
        },
        {   where: {
            id: req.params.id
            }
        }
    ).then( (user) => {
            return res.redirect('/dashboard/users');
        }).catch(error => {
            console.log(error);
            res.redirect('/signup');
        });
})

router.put('/bio/:id',(req,res)=>{
    const {first_name, last_name, kota} = req.body
    db.User_bio.update({
        first_name,
        last_name,
        kota
        },
        {   where: {
                id: req.params.id
            }
        }
    ).then( (user) => {
            return res.redirect('/dashboard/bio');
        }).catch(error => {
            console.log(error);
            res.redirect('/signup');
        });
})

router.delete('/users/:id', async (req, res) => {
    await db.Users.destroy({
      where:{
        id:req.params.id
      },
      include: [{ model: db.User_bio},{model: db.User_history}]
    }).then(() => {
      res.status(201).redirect('/dashboard/users')
    }).catch(err => {
      res.status(400).json(`Can't delete article - ${err.message}`)
    })
  })

  router.delete('/history/:id', async (req, res) => {
    await db.User_history.destroy({
      where:{
        id:req.params.id
      },
      include: [db.User_bio, db.User_history]
    }).then(() => {
      res.status(201).redirect('/dashboard/history')
    }).catch(err => {
      res.status(400).json(`Can't delete article - ${err.message}`)
    })
  })



router.get('/bio',(req,res)=>{
    res.render('bio')
})
router.get('/history',(req,res)=>{
    res.render('history')//
})

module.exports = router