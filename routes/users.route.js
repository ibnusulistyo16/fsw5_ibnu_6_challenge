const express = require('express');
const router = express.Router()
const db = require("../models/index")

const redirectDashboard = (req,res,next)=>{
    if(req.session.userId){
        res.redirect('/dashboard')
    }else{
        next();
    }
}

//ketika klik login di form login masuk ke sini untuk masuk ke dashboard atau game
router.post('/login',redirectDashboard, (req, res) => {
    const {username, password} = req.body;
    db.Users.findOne({
        where: {
            username: username,
            password: password
        }
    })
        .then((user) => {
            // user = user.map(i => i.get({ plain: true}))[0];
            req.session.userId = user.id;
            // console.log(`userrouter id : . ${user.id}`)
            if (user.level== 'admin') {
                res.redirect('/dashboard');
            } if(user.level=='member') {// diantar kembali ke router utama localhost:/dashboard
                 res.redirect('/game');
            }
        })
        .catch((err) => {
            res.redirect('/login');
        })
});

//ketika klik register/signup masuk ke sini untuk menyimpan data
router.post('/register',redirectDashboard,(req,res)=>{
    const {first_name, last_name, kota, email, username, password} = req.body
    db.Users.create({
        email,
        username,
        password,
        level: "member",
        User_bio:{
                first_name,
                last_name,
                kota
            }
        },
        {
            include:[{
                model:db.User_bio
            }]
        }
    ).then( (user) => {
            return res.redirect('/dashboard');
        }).catch(error => {
            console.log(error);
            res.redirect('/signup');
        });
})
module.exports = router