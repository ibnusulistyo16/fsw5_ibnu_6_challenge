'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_history.belongsTo(models.Users, {
        foreignKey: {
          name: 'user_id',
          as:'Users'
        }
      })
    }
  };
  User_history.init({
    user_id: DataTypes.STRING,
    score_user: DataTypes.INTEGER,
    score_comp: DataTypes.INTEGER,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_history',
  });
  return User_history;
};