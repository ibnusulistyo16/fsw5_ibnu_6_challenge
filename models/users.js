'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Users.hasOne(models.User_bio,{
        foreignKey: {
           name: 'user_id',
           as: "User_id"
        }
      })

    Users.hasMany(models.User_history, {
      foreignKey: {
        name: 'user_id',
        as:"User_history"
      }
    })
      // define association here
    }
  };
  Users.init({
    username: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    level: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};