'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_bio extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_bio.hasMany(models.Users, {
        foreignKey: {
          name: 'id',
          as:"Users"
        }
      })
    }
  };
  User_bio.init({
    user_id: DataTypes.INTEGER,
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    kota: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_bio',
  });
  return User_bio;
};